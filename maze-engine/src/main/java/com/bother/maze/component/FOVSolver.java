package com.bother.maze.component;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.bother.maze.BaseObject;
import com.bother.maze.game.GameConstants;
import com.bother.maze.model.Tile;

import static com.bother.maze.game.GameEnums.TileState;
import static com.bother.maze.game.GameEnums.TileType;

/**
 * Field of view algorithm based on
 * recursive shadow casting
 */
// TODO: refactor this shit!
public class FOVSolver extends BaseObject {

    private Tile[][] fovMap;

    private int mapWidth;
    private int mapHeight;
    private int fromTileX;
    private int fromTileY;
    private int fovRadSqr = GameConstants.FOV_RADIUS * GameConstants.FOV_RADIUS;

    private Paint paint;

    public FOVSolver(Tile[][] fovMap) {
        this.fovMap = fovMap;
        mapHeight = fovMap.length;
        mapWidth = fovMap[0].length;

        paint = new Paint();
        paint.setARGB(40, 40, 40, 40);

        // initialize fov
        fromTileX = (int) systemRegistry.levelManager.getCurrentLevel().getWorld().getStart().x / GameConstants.TILE_SIZE;
        fromTileY = (int) systemRegistry.levelManager.getCurrentLevel().getWorld().getStart().y / GameConstants.TILE_SIZE;
        clearFov();
        calculateFov();
    }

    @Override
    public void update() {
        // if position hasn't changed, no update required
        Camera cam = systemRegistry.camera;
        if (fromTileX == cam.getTileX() && fromTileY == cam.getTileY()) return;

        fromTileX = cam.getTileX();
        fromTileY = cam.getTileY();

        calculateFov();
    }

    public void render(Canvas canvas) {
        Camera cam = systemRegistry.camera;
        int bottom = Math.max(fromTileY - GameConstants.FOV_RADIUS, 0);
        int top = Math.min(fromTileY + GameConstants.FOV_RADIUS, mapHeight);
        int left = Math.max(fromTileX - GameConstants.FOV_RADIUS, 0);
        int right = Math.min(fromTileX + GameConstants.FOV_RADIUS, mapWidth);

        for (int i = bottom; i < top; i++) {
            for (int j = left; j < right; j++) {

                if (fovMap[i][j].isStateOf(TileState.DARK)) continue;

                float renderX = fovMap[i][j].getPosition().x - cam.position.x;
                float renderY = fovMap[i][j].getPosition().y - cam.position.y;

                canvas.drawRect(renderX, renderY, renderX + GameConstants.TILE_SIZE, renderY + GameConstants.TILE_SIZE, paint);
            }
        }
    }

    private void calculateFov() {
        // TODO: add more clear fov strategies

        clearFov();
        fovMap[fromTileY][fromTileX].light();

        scan1(1, 1, 0);
        scan2(1, 1, 0);
        scan3(1, 1, 0);
        scan4(1, 1, 0);
        scan5(1, 1, 0);
        scan6(1, 1, 0);
        scan7(1, 1, 0);
        scan8(1, 1, 0);
    }

    private void clearFov() {
        int bottom = Math.max(fromTileY - GameConstants.FOV_RADIUS, 0);
        int top = Math.min(fromTileY + GameConstants.FOV_RADIUS, mapHeight);
        int left = Math.max(fromTileX - GameConstants.FOV_RADIUS, 0);
        int right = Math.min(fromTileX + GameConstants.FOV_RADIUS, mapWidth);

        for (int i = bottom; i < top; i++) {
            for (int j = left; j < right; j++) {
                fovMap[i][j].dim();
            }
        }
    }

    private boolean keepRecursion(int x, int y, int depth) {
        if (x < 0)
            x = 0;
        else if (x >= mapWidth)
            x = mapWidth - 1;

        if (y < 0)
            y = 0;
        else if (y >= mapHeight)
            y = mapHeight - 1;

        return (depth < GameConstants.FOV_RADIUS && !fovMap[y][x].isTypeOf(TileType.WALL));
    }

    private double getSlope(double pX1, double pY1, double pX2, double pY2, boolean pInvert) {
        if (pInvert)
            return (pY1 - pY2) / (pX1 - pX2);
        else
            return (pX1 - pX2) / (pY1 - pY2);
    }

    private int getVisDistance(int pX1, int pY1, int pX2, int pY2) {
        return (pX1 - pX2) * (pX1 - pX2) + (pY1 - pY2) * (pY1 - pY2);
    }

    private void scan1(int pDepth, double pStartSlope, double pEndSlope) {
        int y = fromTileY - pDepth;
        if (y < 0) return;

        int x = fromTileX - (int) (pStartSlope * pDepth);
        if (x < 0) x = 0;

        while (getSlope(x, y, fromTileX, fromTileY, false) >= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (x - 1 >= 0 && !fovMap[y][x - 1].isTypeOf(TileType.WALL))
                        scan1(pDepth + 1, pStartSlope, getSlope(x - 0.5, y + 0.5, fromTileX, fromTileY, false));
                } else {
                    if (x - 1 >= 0 && fovMap[y][x - 1].isTypeOf(TileType.WALL))
                        pStartSlope = getSlope(x - 0.5, y - 0.5, fromTileX, fromTileY, false);

                    fovMap[y][x].light();
                }
            }
            x++;
        }
        x--;

        if (keepRecursion(x, y, pDepth))
            scan1(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan2(int pDepth, double pStartSlope, double pEndSlope) {
        int y = fromTileY - pDepth;
        if (y < 0) return;

        int x = fromTileX + (int) (pStartSlope * pDepth);
        if (x >= mapWidth) x = mapWidth - 1;

        while (getSlope(x, y, fromTileX, fromTileY, false) <= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (x + 1 < mapWidth && !fovMap[y][x + 1].isTypeOf(TileType.WALL))
                        scan2(pDepth + 1, pStartSlope, getSlope(x + 0.5, y + 0.5, fromTileX, fromTileY, false));
                } else {
                    if (x + 1 < mapWidth && fovMap[y][x + 1].isTypeOf(TileType.WALL))
                        pStartSlope = -getSlope(x + 0.5, y - 0.5, fromTileX, fromTileY, false);

                    fovMap[y][x].light();
                }
            }
            x--;
        }
        x++;
        if (keepRecursion(x, y, pDepth))
            scan2(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan3(int pDepth, double pStartSlope, double pEndSlope) {
        int x = fromTileX + pDepth;
        if (x >= mapWidth) return;

        int y = fromTileY - (int) (pStartSlope * pDepth);
        if (y < 0) y = 0;

        while (getSlope(x, y, fromTileX, fromTileY, true) <= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (y - 1 >= 0 && !fovMap[y - 1][x].isTypeOf(TileType.WALL))
                        scan3(pDepth + 1, pStartSlope, getSlope(x - 0.5, y - 0.5, fromTileX, fromTileY, true));
                } else {
                    if (y - 1 >= 0 && fovMap[y - 1][x].isTypeOf(TileType.WALL))
                        pStartSlope = -getSlope(x + 0.5, y - 0.5, fromTileX, fromTileY, true);

                    fovMap[y][x].light();
                }
            }
            y++;
        }
        y--;

        if (keepRecursion(x, y, pDepth))
            scan3(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan4(int pDepth, double pStartSlope, double pEndSlope) {
        int x = fromTileX + pDepth;
        if (x >= mapWidth) return;

        int y = fromTileY + (int) (pStartSlope * pDepth);
        if (y >= mapHeight) y = mapHeight - 1;

        while (getSlope(x, y, fromTileX, fromTileY, true) >= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (y + 1 < mapHeight && !fovMap[y + 1][x].isTypeOf(TileType.WALL))
                        scan4(pDepth + 1, pStartSlope, getSlope(x - 0.5, y + 0.5, fromTileX, fromTileY, true));
                } else {
                    if (y + 1 < mapHeight && fovMap[y + 1][x].isTypeOf(TileType.WALL))
                        pStartSlope = getSlope(x + 0.5, y + 0.5, fromTileX, fromTileY, true);

                    fovMap[y][x].light();
                }
            }
            y--;
        }
        y++;

        if (keepRecursion(x, y, pDepth))
            scan4(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan5(int pDepth, double pStartSlope, double pEndSlope) {
        int y = fromTileY + pDepth;
        if (y >= mapHeight) return;

        int x = fromTileX + (int) (pStartSlope * pDepth);
        if (x >= mapWidth) x = mapWidth - 1;

        while (getSlope(x, y, fromTileX, fromTileY, false) >= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (x + 1 < mapWidth && !fovMap[y][x + 1].isTypeOf(TileType.WALL))
                        scan5(pDepth + 1, pStartSlope, getSlope(x + 0.5, y - 0.5, fromTileX, fromTileY, false));
                } else {
                    if (x + 1 < mapWidth && fovMap[y][x + 1].isTypeOf(TileType.WALL))
                        pStartSlope = getSlope(x + 0.5, y + 0.5, fromTileX, fromTileY, false);

                    fovMap[y][x].light();
                }
            }
            x--;
        }
        x++;

        if (keepRecursion(x, y, pDepth))
            scan5(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan6(int pDepth, double pStartSlope, double pEndSlope) {
        int y = fromTileY + pDepth;
        if (y >= mapHeight) return;

        int x = fromTileX - (int) (pStartSlope * pDepth);
        if (x < 0) x = 0;

        while (getSlope(x, y, fromTileX, fromTileY, false) <= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (x - 1 >= 0 && !fovMap[y][x - 1].isTypeOf(TileType.WALL))
                        scan6(pDepth + 1, pStartSlope, getSlope(x - 0.5, y - 0.5, fromTileX, fromTileY, false));
                } else {
                    if (x - 1 >= 0 && fovMap[y][x - 1].isTypeOf(TileType.WALL))
                        pStartSlope = -getSlope(x - 0.5, y + 0.5, fromTileX, fromTileY, false);

                    fovMap[y][x].light();
                }
            }
            x++;
        }
        x--;

        if (keepRecursion(x, y, pDepth))
            scan6(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan7(int pDepth, double pStartSlope, double pEndSlope) {
        int x = fromTileX - pDepth;
        if (x < 0) return;

        int y = fromTileY + (int) (pStartSlope * pDepth);
        if (y >= mapHeight) y = mapHeight - 1;

        while (getSlope(x, y, fromTileX, fromTileY, true) <= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (y + 1 < mapHeight && !fovMap[y + 1][x].isTypeOf(TileType.WALL))
                        scan7(pDepth + 1, pStartSlope, getSlope(x + 0.5, y + 0.5, fromTileX, fromTileY, true));
                } else {
                    if (y + 1 < mapHeight && fovMap[y + 1][x].isTypeOf(TileType.WALL))
                        pStartSlope = -getSlope(x - 0.5, y + 0.5, fromTileX, fromTileY, true);

                    fovMap[y][x].light();
                }
            }
            y--;
        }
        y++;

        if (keepRecursion(x, y, pDepth))
            scan7(pDepth + 1, pStartSlope, pEndSlope);
    }

    private void scan8(int pDepth, double pStartSlope, double pEndSlope) {
        int x = fromTileX - pDepth;
        if (x < 0) return;

        int y = fromTileY - (int) (pStartSlope * pDepth);
        if (y < 0) y = 0;

        while (getSlope(x, y, fromTileX, fromTileY, true) >= pEndSlope) {
            if (getVisDistance(x, y, fromTileX, fromTileY) <= fovRadSqr) {
                if (fovMap[y][x].isTypeOf(TileType.WALL)) {
                    if (y - 1 >= 0 && !fovMap[y - 1][x].isTypeOf(TileType.WALL))
                        scan8(pDepth + 1, pStartSlope, getSlope(x + 0.5, y - 0.5, fromTileX, fromTileY, true));
                } else {
                    if (y - 1 >= 0 && fovMap[y - 1][x].isTypeOf(TileType.WALL))
                        pStartSlope = getSlope(x - 0.5, y - 0.5, fromTileX, fromTileY, true);

                    fovMap[y][x].light();
                }
            }
            y++;
        }
        y--;

        if (keepRecursion(x, y, pDepth))
            scan8(pDepth + 1, pStartSlope, pEndSlope);
    }
}
