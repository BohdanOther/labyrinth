package com.bother.maze.component;

import com.bother.maze.BaseObject;
import com.bother.maze.GameObject;
import com.bother.maze.common.Vector2;
import com.bother.maze.game.GameConstants;

/**
 * Created by Bohdan on 29.01.2015.
 */
public class Camera extends BaseObject {

    public Vector2 position;

    private Vector2 target;
    private GameObject targetObject;

    private int tileX;
    private int tileY;

    public Camera() {
        position = new Vector2();
        target = new Vector2();
    }

    @Override
    public void update() {
        target.set(targetObject.getCenterX() - systemRegistry.contextParams.scrWidth / 2,
                targetObject.getCenterY() - systemRegistry.contextParams.scrHeight / 2);

        // TODO: fix this magic constants
        if (Math.abs(target.x - position.x) > 600) {
            position.x = target.x + 150;
        }

        if (Math.abs(target.y - position.y) > 600) {
            position.y = target.y + 150;
        }

        if (Math.abs(target.x - position.x) > 40) {
            if (position.x < target.x) {
                position.x += GameConstants.CAMERA_FOLLOW_SPEED;
            } else if (position.x > target.x) {
                position.x -= GameConstants.CAMERA_FOLLOW_SPEED;
            }
        }

        if (Math.abs(target.y - position.y) > 40) {
            if (position.y < target.y) {
                position.y += GameConstants.CAMERA_FOLLOW_SPEED;
            } else if (position.y > target.y) {
                position.y -= GameConstants.CAMERA_FOLLOW_SPEED;
            }
        }

        // TODO : what is this for christ's sake ?
        position.x = Math.round(position.x);
        position.y = Math.round(position.y);

        tileX = (int) targetObject.getPosition().x / GameConstants.TILE_SIZE;
        tileY = (int) targetObject.getPosition().y / GameConstants.TILE_SIZE;
    }

    public int getTileX() {
        return tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public void setTargetObject(GameObject object) {
        targetObject = object;
    }

    public GameObject getTargetObject() {
        return targetObject;
    }
}
