package com.bother.maze.component;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Shader;

import com.bother.maze.BaseObject;
import com.bother.maze.common.Vector2;
import com.bother.maze.game.GameConstants;
import com.bother.maze.game.GameEnums;
import com.bother.maze.model.Tile;
import com.bother.maze_engine.R;

/**
 * Created by Bohdan on 06.02.2015.
 */
// TODO: do something with this...
public class ShadowCaster extends BaseObject {

    private Tile[][] map;
    private int mapHeight;
    private int mapWidth;

    private float shadowsRadius = 500;
    private float shadowRadius2 = shadowsRadius * shadowsRadius;

    private Vector2 light;
    private Path shadow;
    private Paint shadowPaint;

    private final Paint shadowMaskPaint;
    private final Bitmap shadowMaskBitmap;

    private Matrix shadowMapTransformMatrix;
    Shader r;

    public ShadowCaster(Tile[][] shadowMap) {
        map = shadowMap;
        mapHeight = shadowMap.length;
        mapWidth = shadowMap[0].length;

        light = new Vector2();
        shadow = new Path();
        shadowPaint = new Paint();

        shadowPaint.setColor(Color.argb(20, 40, 40, 40));

        shadowMaskPaint = new Paint();
        shadowMaskPaint.setColor(Color.argb(30, 100, 100, 100));
        // shadowMaskPaint.setXfermode(
        //         new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));

        shadowMaskBitmap = BitmapFactory.decodeResource(
                systemRegistry.contextParams.resources, R.drawable.shadow_transparent);
        r = new RadialGradient(light.x, light.y, shadowsRadius, Color.DKGRAY, Color.WHITE, Shader.TileMode.CLAMP);
        //shadowMaskPaint.setShader(r);
        shadowMapTransformMatrix = new Matrix();
    }

    public void update(final float fromX, final float fromY) {
        light.set(fromX, fromY);

        shadowMapTransformMatrix.reset();
        float drawX = light.x - systemRegistry.camera.position.x;
        float drawY = light.y - systemRegistry.camera.position.y;
        shadowMapTransformMatrix.postTranslate(drawX, drawY);
        r.setLocalMatrix(shadowMapTransformMatrix);
    }

    public void render(Canvas canvas, Camera cam) {

//        canvas.drawBitmap(
//                shadowMaskBitmap,
//                shadowMapTransformMatrix,
//                shadowMaskPaint);
//
//        canvas.drawCircle(light.x - cam.getPosition().x, light.y - cam.getPosition().y,
//                shadowsRadius,
//                shadowPaint);

        int tileX = systemRegistry.camera.getTileX();
        int tileY = systemRegistry.camera.getTileY();

        int bottom = Math.max(tileY - GameConstants.FOV_RADIUS, 1);
        int top = Math.min(tileY + GameConstants.FOV_RADIUS, mapHeight - 1);
        int left = Math.max(tileX - GameConstants.FOV_RADIUS, 1);
        int right = Math.min(tileX + GameConstants.FOV_RADIUS, mapWidth - 1);

        Tile t;

        for (int i = bottom; i < top; i++) {
            for (int j = left; j < right; j++) {
                t = map[i][j];

                if (t.isStateOf(GameEnums.TileState.BLOCKER)) {
                    shadow.reset();

                    // top
                    if ((!t.getSideBlockers().contains(GameEnums.Direction.UP) || map[i - 1][j].isStateOf(GameEnums.TileState.DARK))
                            && light.y > t.getPosition().y) {

                        float dist2ToCaster = (float)
                                (Math.pow(light.x - t.getRenderX(), 2) + Math.pow(light.y - t.getRenderY(), 2));

                        if (dist2ToCaster < shadowRadius2) {

                            shadow.moveTo(t.getRenderX(), t.getRenderY());
                            shadow.lineTo(t.getRenderX() + t.getRenderWidth(), t.getRenderY());
                            addLineToShadowPath(t.getRenderX() + t.getRenderWidth(), t.getRenderY(), dist2ToCaster);
                            addLineToShadowPath(t.getRenderX(), t.getRenderY(), dist2ToCaster);
                            shadow.close();
                        }
                    }
                }

                // bottom
                if ((!t.getSideBlockers().contains(GameEnums.Direction.DOWN) || map[i + 1][j].isStateOf(GameEnums.TileState.DARK))
                        && light.y < t.getPosition().y + t.height) {

                    canvas.drawLine(light.x - cam.position.x,
                            light.y - cam.position.y,
                            t.getRenderX() - cam.position.x,
                            t.getRenderY() + t.getRenderHeight() - cam.position.y, shadowPaint);

                    canvas.drawLine(light.x - cam.position.x,
                            light.y - cam.position.y,
                            t.getRenderX() + t.getRenderWidth() - cam.position.x,
                            t.getRenderY() + t.getRenderHeight() - cam.position.y, shadowPaint);

                }

                // left
                if ((!t.getSideBlockers().contains(GameEnums.Direction.LEFT) || map[i][j - 1].isStateOf(GameEnums.TileState.DARK))
                        && light.x > t.getPosition().x) {


                }

                // right
                if ((!t.getSideBlockers().contains(GameEnums.Direction.RIGHT) || map[i][j + 1].isStateOf(GameEnums.TileState.DARK))
                        && light.x < t.getPosition().x + t.width) {


                }

                shadow.offset(-cam.position.x, -cam.position.y);
                canvas.drawPath(shadow, shadowMaskPaint);
            }
        }
    }


    private void addLineToShadowPath(float x, float y, float dist2ToCaster) {
        final float vecX = x - light.x;
        final float vecY = y - light.y;
        final float distToCaster = (float) Math.sqrt(dist2ToCaster);
        final float shadowDist = shadowsRadius - distToCaster;
        final float shadowX = x + vecX / distToCaster * shadowDist;
        final float shadowY = y + vecY / distToCaster * shadowDist;

        shadow.lineTo(shadowX, shadowY);
    }

}
