package com.bother.maze.generation;

import android.util.Log;

import java.util.Random;

/**
 * Created by urion on 20.12.15.
 */
public class Generator {
    private int Width, Height;
    Random randomNumberGenerator = new Random();

    public int[][] GetMaze(int MazeSize, Algorithm AlgorithmType) {
        int size = (int) (MazeSize % 2 == 0 ? MazeSize + 1 : MazeSize);

        Width = Height = size;

        if (AlgorithmType == null)
            AlgorithmType = Algorithm.SideWinder;

        switch (AlgorithmType) {
            case SideWinder:
                return GetSideWinderMaze();
            default:
                return null;
        }
    }

    private int nextInt(int from, int to) {
        // Log.d("act1","args: " + from + " " + to);
        if (from > to) throw new IllegalArgumentException("kinda strange args: " + from + " " + to);
        return randomNumberGenerator.nextInt(to - from + 1) + from;
    }

    private int[][] GetEllerMaze() {
        return null;
    }

    private int[][] GetSideWinderMaze() {
        //creating base of the matrix with first empty row
        int[][] mazeField = new int[Height][];
        for (int i = 0; i < Height; i++) {
            mazeField[i] = new int[Width];
        }

        for (int rowNumber = 1; rowNumber < Height; rowNumber++)
            for (int columnNumber = 0; columnNumber < Width; columnNumber++)
                mazeField[rowNumber][columnNumber] = 1;


        for (int rowNumber = 2; rowNumber < Height; rowNumber += 2) {
            boolean isCurrentRawCompleted = false;
            int currentColumnNumber = 0;
            while (!isCurrentRawCompleted) {
                int cellsAmount = nextInt(1, 5);
                assert cellsAmount > 0;
                int cellsRightSide = Math.min(currentColumnNumber + cellsAmount * 2, Width - 1);

                for (int columnNumber = currentColumnNumber; columnNumber <= cellsRightSide; columnNumber++) {
                    mazeField[rowNumber][columnNumber] = 0;
                }

                int TopCarvePoint = Math.min(currentColumnNumber + nextInt(1, cellsAmount) * 2, Width - 1);

                mazeField[rowNumber - 1][TopCarvePoint] = 0;

                currentColumnNumber = cellsRightSide + 2;

                if (currentColumnNumber >= Height)
                    isCurrentRawCompleted = true;
            }
        }

        mazeField = AddStartAndFinish(mazeField);
        mazeField = AddBoundaries(mazeField);

        //PrintTheMatrix(mazeField);
        return mazeField;
    }

    private int[][] AddBoundaries(int[][] input) {
        int[][] resultMatrix = new int[Height + 2][];
        for (int i = 0; i < Height + 2; i++) {
            resultMatrix[i] = new int[Width + 2];
        }

        for (int rowNumber = 0; rowNumber < Height + 2; rowNumber++) {
            resultMatrix[rowNumber][0] = resultMatrix[rowNumber][Width + 1] = 1;
        }

        for (int columnNumber = 0; columnNumber < Width + 2; columnNumber++) {
            resultMatrix[0][columnNumber] = resultMatrix[Height + 1][columnNumber] = 1;
        }
        for (int rowNumber = 0; rowNumber < Height; rowNumber++)
            for (int columnNumber = 0; columnNumber < Width; columnNumber++)
                resultMatrix[rowNumber + 1][columnNumber + 1] = input[rowNumber][columnNumber];
        Width += 2;
        Height += 2;
        return resultMatrix;
    }

    private int[][] AddStartAndFinish(int[][] input) {
        //
        //    0 | 3
        //    -----
        //    1 | 2
        //
        int startSquare = nextInt(0, 3);
        int finishSquare = (startSquare + 2) % 4;

        int StartXPosition, StartYPosition, FinishXPosition, FinishYPosition;

        boolean needChangePosition = true;
        while (needChangePosition) {
            StartXPosition = nextInt(0, Width / 2);
            if (startSquare == 3 || startSquare == 2) StartXPosition += Width / 2;

            StartYPosition = nextInt(0, Width / 2);
            if (startSquare == 1 || startSquare == 2) StartYPosition += Width / 2;

            if (input[StartYPosition][StartXPosition] == 0) {
                needChangePosition = false;
                input[StartYPosition][StartXPosition] = 2;
            }
        }
        needChangePosition = true;
        while (needChangePosition) {
            FinishXPosition = nextInt(0, Width / 2);
            if (finishSquare == 3 || finishSquare == 2) FinishXPosition += Width / 2;

            FinishYPosition = nextInt(0, Width / 2);
            if (finishSquare == 1 || finishSquare == 2) FinishYPosition += Width / 2;

            if (input[FinishYPosition][FinishXPosition] == 0) {
                needChangePosition = false;
                input[FinishYPosition][FinishXPosition] = 3;
            }
        }
        return input;
    }

    private void PrintTheMatrix(int[][] matrix) {
        for (int i = 0; i < Height; i++) {
            System.out.println();
            for (int j = 0; j < Width; j++) {
                switch (matrix[i][j]) {
                    case 0:
                        System.out.print('.');
                        break;
                    case 1:
                        System.out.print('#');
                        break;
                    case 2:
                        System.out.print('S');
                        break;
                    case 3:
                        System.out.print('F');
                        break;
                }
            }
        }

    }
}

