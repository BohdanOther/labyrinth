package com.bother.maze;

import android.content.Context;

import com.bother.maze.generation.Generator;
import com.bother.maze.model.Level;
import com.google.gson.Gson;

/**
 * Created by Bohdan on 19.03.2015.
 */
public class LevelManager {

    public static class LevelData {
        public final boolean completed;
        public final int[][] tiles;

        public LevelData(boolean completed, int[][] tiles) {
            this.completed = completed;
            this.tiles = tiles;
        }
    }

    private Level currentLevel;

    public void LoadLevelFromFile(Context context, String levelName) {
        Gson gson = new Gson();
        int id = context.getResources().getIdentifier(levelName, "string", context.getPackageName());

        if (id != 0) {
            String json_level_data = context.getResources().getString(id);
            LevelData data = gson.fromJson(json_level_data, LevelData.class);

            Level level = new Level(data);
            currentLevel = level;
            spawnObjects(data.tiles);
        } else {
            // TODO: handle no level found somehow
        }
    }

    public void LoadLevelFromGenerator(int levelSize) {
        Generator generator = new Generator();
        LevelData data = new LevelData(true, generator.GetMaze(levelSize, null));
        Level level = new Level(data);
        currentLevel = level;
        spawnObjects(data.tiles);
    }


    public void ReloadLevel() {

    }

    public Level getCurrentLevel() {
        return currentLevel;
    }

    public void spawnObjects(int[][] world) {
        BaseObject.systemRegistry.gameObjectFactory.spawnFromWorld(world);
    }
}

