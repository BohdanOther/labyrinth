package com.bother.maze.model;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.bother.maze.GameObject;
import com.bother.maze.common.AABBoxCollisionVolume;
import com.bother.maze.component.Camera;
import com.bother.maze.game.GameConstants;

import java.util.EnumSet;

import static com.bother.maze.game.GameEnums.Direction;
import static com.bother.maze.game.GameEnums.TileState;
import static com.bother.maze.game.GameEnums.TileType;

/**
 * Created by Bohdan on 24.01.2015.
 */
public class Tile extends GameObject {

    public int tilePosX;
    public int tilePosY;

    private int renderWidth;
    private int renderHeight;

    private TileState state;
    private TileType type;

    private EnumSet<Direction> sideBlockers;

    // TODO: stupid shit. get this out of here
    public boolean hunted;
    private Paint huntedPaint;

    private Paint testPaint;

    public Tile(TileType type) {
        this.type = type;

        sideBlockers = EnumSet.noneOf(Direction.class);


        huntedPaint = new Paint();
        huntedPaint.setColor(0x88f1555f);

        testPaint = new Paint();
        testPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        switch (type) {
            case WALL:
                testPaint.setColor(GameConstants.WALL_COLOR);
                break;

            case FINISH:
                testPaint.setColor(GameConstants.FINISH_COLOR);
            default:
                break;
        }
    }

    public void light() {
        state = TileState.LIT;

        tilePosX = (int) getPosition().x / GameConstants.TILE_SIZE;
        tilePosY = (int) getPosition().y / GameConstants.TILE_SIZE;

        for (Direction d : getSideBlockers()) {
            systemRegistry.levelManager.getCurrentLevel().getTiles()[tilePosY + d.y][tilePosX + d.x].state = TileState.BLOCKER;
        }
    }

    public void dim() {
        state = TileState.DARK;
    }

    public boolean isTypeOf(final TileType type) {
        return this.type == type;
    }

    public boolean isStateOf(final TileState state) {
        return this.state == state;
    }

    @Override
    public void update() {
        if (state == TileState.DARK) {
            if (renderWidth > 0) {
                renderWidth -= GameConstants.TILE_FADE_SPEED;
                renderHeight -= GameConstants.TILE_FADE_SPEED;
            }
        } else {
            if (renderWidth < GameConstants.TILE_SIZE) {
                renderHeight += GameConstants.TILE_FADE_SPEED;
                renderWidth += GameConstants.TILE_FADE_SPEED;
            }
        }
    }

    public void render(Canvas canvas) {
        Camera cam = systemRegistry.camera;
        int renderX = (int) (getPosition().x + (width - renderWidth) / 2f - cam.position.x);
        int renderY = (int) (getPosition().y + (height - renderHeight) / 2f - cam.position.y);

        if (renderWidth > 0) {
            switch (type) {
                case WALL:
                    canvas.drawRect(renderX, renderY, renderX + renderWidth, renderY + renderHeight, testPaint);
                    break;
                case FINISH:
                    canvas.drawCircle(getCenterX() - cam.position.x, getCenterY() - cam.position.y,
                            GameConstants.TILE_SIZE / 2 - (width - renderWidth) - 16, testPaint);
                    break;
            }
        }

        if (hunted)
            canvas.drawRect(renderX, renderY, renderX + renderWidth, renderY + renderHeight, huntedPaint);
    }

    public float getRenderWidth() {
        return renderWidth;
    }

    public float getRenderHeight() {
        return renderHeight;
    }

    public float getRenderX() {
        return getPosition().x + (width - renderWidth) / 2f;
    }

    public float getRenderY() {
        return getPosition().y + (height - renderHeight) / 2f;
    }

    public EnumSet<Direction> getSideBlockers() {
        return sideBlockers;
    }
}
