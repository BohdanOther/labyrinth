package com.bother.maze.model;

import android.graphics.Canvas;

import com.bother.maze.BaseObject;
import com.bother.maze.GameObjectFactory;
import com.bother.maze.common.Vector2;
import com.bother.maze.component.Camera;
import com.bother.maze.game.GameConstants;
import com.bother.maze.game.GameEnums;

/**
 * Created by Bohdan on 24.01.2015.
 */
public class TiledWorld extends BaseObject {

    private int worldWidth;
    private int worldHeight;

    private Vector2 start;
    private Vector2 finish;

    private Tile[][] tileMap;

    public TiledWorld(int[][] tiles) {
        loadMap(tiles);
    }

    private void loadMap(int[][] tiles) {
        worldWidth = tiles[0].length;
        worldHeight = tiles.length;

        tileMap = new Tile[worldHeight][worldWidth];

        GameObjectFactory factory = BaseObject.systemRegistry.gameObjectFactory;

        for (int i = 0; i < worldHeight; i++) {
            for (int j = 0; j < worldWidth; j++) {

                int id = tiles[i][j];
                // GameEnums.TileType type = GameEnums.TileType.values()[id];

                float posX = j * GameConstants.TILE_SIZE;
                float posY = i * GameConstants.TILE_SIZE;

                tileMap[i][j] = (Tile) factory.spawn(GameObjectFactory.GameObjectType.idToType(id), posX, posY);

                switch (id) {
                    case 3:
                        finish = new Vector2(j * GameConstants.TILE_SIZE, i * GameConstants.TILE_SIZE);
                        break;
                    case 2:
                        start = new Vector2(j * GameConstants.TILE_SIZE, i * GameConstants.TILE_SIZE);
                        break;
                }

                // Calculate side blockers for tile
                if (id != 3 && i != 0 && j != 0 && i != worldHeight - 1 && j != worldWidth - 1) {
                    for (GameEnums.Direction d : GameEnums.Direction.all()) {
                        if (tiles[i + d.y][j + d.x] == 1) {
                            tileMap[i][j].getSideBlockers().add(d);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void update() {
        Camera cam = systemRegistry.camera;
        int bottom = Math.max(cam.getTileY() - GameConstants.RENDER_RADIUS, 0);
        int top = Math.min(cam.getTileY() + GameConstants.RENDER_RADIUS + 1, tileMap.length);
        int left = Math.max(cam.getTileX() - GameConstants.RENDER_RADIUS, 0);
        int right = Math.min(cam.getTileX() + GameConstants.RENDER_RADIUS + 1, tileMap[0].length);

        for (int i = bottom; i < top; i++) {
            for (int j = left; j < right; j++) {
                tileMap[i][j].update();
            }
        }
    }

    public void render(Canvas canvas) {
        Camera cam = systemRegistry.camera;
        int bottom = Math.max(cam.getTileY() - GameConstants.RENDER_RADIUS, 0);
        int top = Math.min(cam.getTileY() + GameConstants.RENDER_RADIUS + 1, tileMap.length);
        int left = Math.max(cam.getTileX() - GameConstants.RENDER_RADIUS, 0);
        int right = Math.min(cam.getTileX() + GameConstants.RENDER_RADIUS + 1, tileMap[0].length);

        for (int i = bottom; i < top; i++) {
            for (int j = left; j < right; j++) {
                tileMap[i][j].render(canvas);
            }
        }
    }

    public int getWidth() {
        return worldWidth;
    }

    public int getHeight() {
        return worldHeight;
    }

    public Vector2 getStart() {
        return start;
    }

    public Vector2 getFinish() {
        return finish;
    }

    public Tile[][] getTiles() {
        return tileMap;
    }

    public Tile getTile(int row, int col) {
        return tileMap[row][col];
    }
}