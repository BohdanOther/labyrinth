package com.bother.maze.model;

import android.graphics.Point;

import com.bother.maze.GameObject;
import com.bother.maze.common.Vector2;
import com.bother.maze.game.GameConstants;
import com.bother.maze.game.GameEnums;

import java.util.ArrayList;

/**
 * Created by Bohdan on 25.02.2015.
 */
public class Hunter extends GameObject {

    private ArrayList<Point> map;

    private final static GameEnums.Direction[] hunterDirections =
            {
                    GameEnums.Direction.LEFT,
                    GameEnums.Direction.RIGHT,
                    GameEnums.Direction.UP,
                    GameEnums.Direction.DOWN
            };

    private int last_count;
    private int huntDelay = 100;
    private int lastHuntTime;

    public Hunter() {
        map = new ArrayList<>();
    }

    public void update() {
        if (lastHuntTime >= huntDelay) {

            last_count = map.size();
            for (int i = 0; i < last_count; i++) {
                hunt(map.get(i));
            }

            map.subList(0, last_count).clear();
            lastHuntTime = 0;
        } else {
            lastHuntTime++;
        }
    }

    private void hunt(Point p) {
        for (GameEnums.Direction d : hunterDirections) {
            Tile t = systemRegistry.levelManager.getCurrentLevel().getWorld().getTile(p.y + d.y, p.x + d.x);

            if (t.isTypeOf(GameEnums.TileType.FLOOR) && !t.hunted) {
                map.add(new Point(p.x + d.x, p.y + d.y));
                t.hunted = true;
            }
        }
    }

    @Override
    public void setPosition(Vector2 position) {
        getPosition().set(position);
        map.add(new Point((int) position.x / GameConstants.TILE_SIZE, (int) position.y / GameConstants.TILE_SIZE));
    }
}
