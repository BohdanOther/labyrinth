package com.bother.maze.model;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.bother.maze.GameObject;
import com.bother.maze.common.AABBoxCollisionVolume;
import com.bother.maze.common.Vector2;
import com.bother.maze.component.Camera;
import com.bother.maze.game.GameConstants;
import com.bother.maze.game.GameEnums;

/**
 * Created by Bohdan on 23.01.2015.
 */
public class Player extends GameObject {

    private Vector2 correction;
    private Paint p;

    public Player() {
        correction = new Vector2();
        p = new Paint();
        p.setColor(GameConstants.PLAYER_COLOR);
    }

    @Override
    public void update() {
        setVelocity(systemRegistry.inputInterface.controller.getVelocity());
        if (isMoving()) {
            move(getVelocity());
        }
    }

    public void render(Canvas canvas) {
        Camera cam = systemRegistry.camera;
        float renderX = getPosition().x - cam.position.x;
        float renderY = getPosition().y - cam.position.y;

        canvas.drawRect(renderX, renderY, renderX + width, renderY + height, p);
    }

    private void move(final Vector2 delta) {
        AABBoxCollisionVolume collisionVol = getCollisionVolume();

        Vector2 deltaX = new Vector2(delta.x * GameConstants.PLAYER_SPEED, 0);
        Vector2 deltaY = new Vector2(0, delta.y * GameConstants.PLAYER_SPEED);

        collisionVol.moveTo(getPosition());
        collisionVol.translate(deltaX);
        if (!isColliding(collisionVol, correction)) {
            getPosition().add(deltaX);
        } else {
            getPosition().x += correction.x;
        }

        collisionVol.moveTo(getPosition());
        collisionVol.translate(deltaY);
        if (!isColliding(collisionVol, correction)) {
            getPosition().add(deltaY);
        } else {
            getPosition().y += correction.y;
        }
    }

    private boolean isColliding(AABBoxCollisionVolume collisionVolume, Vector2 correction) {
        correction.zero();

        int tileX = (int) getPosition().x / GameConstants.TILE_SIZE;
        int tileY = (int) getPosition().y / GameConstants.TILE_SIZE;

        AABBoxCollisionVolume test;
        TiledWorld world = systemRegistry.levelManager.getCurrentLevel().getWorld();
        //  TODO: This is dangerous on border
        for (GameEnums.Direction d : GameEnums.Direction.all()) {
            test = world.getTile(tileY + d.y, tileX + d.x).getCollisionVolume();
            if (test != null) {
                boolean collided = collisionVolume.isColliding(test);

                if (collided) {
                    int sign = (getPosition().x + width < test.minX() || getPosition().y + height < test.minY()) ? 1 : -1;
                    collisionVolume.moveTo(getPosition());
                    Vector2 dist = collisionVolume.accurateDistance(test);
                    dist.multiply(sign);
                    correction.set(dist);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isMoving() {
        return getVelocity().length2() > 0;
    }
}
