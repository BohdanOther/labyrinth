package com.bother.maze.model;

import android.graphics.Canvas;

import com.bother.maze.BaseObject;
import com.bother.maze.LevelManager;

/**
 * Created by Bohdan on 19.03.2015.
 */
public class Level extends BaseObject {
    private boolean completed;

    private TiledWorld tiledWorld;

    public Level(LevelManager.LevelData data) {
        completed = data.completed;
        tiledWorld = new TiledWorld(data.tiles);
    }

    public void update() {
        tiledWorld.update();
    }

    public void render(Canvas canvas) {
        tiledWorld.render(canvas);
    }

    public Tile[][] getTiles() {
        return tiledWorld.getTiles();
    }

    public TiledWorld getWorld() {
        return tiledWorld;
    }
}
