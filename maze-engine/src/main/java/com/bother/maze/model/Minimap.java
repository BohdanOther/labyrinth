package com.bother.maze.model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.bother.maze.BaseObject;

/**
 * Created by Bohdan on 26.02.2015.
 */
public class Minimap extends BaseObject {

    // map position and size
    private float x;
    private float y;
    private float width = 200;
    private float height = 200;

    private Paint mapPaint;

    public Minimap() {
        mapPaint = new Paint();
        mapPaint.setColor(Color.GRAY);

        x = systemRegistry.contextParams.scrWidth - width - 20;
        y = 20;
    }

    @Override
    public void update() {

    }

    public void render(Canvas canvas) {
        canvas.drawRect(x, y, x + width, y + height, mapPaint);
    }
}
