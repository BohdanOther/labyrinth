package com.bother.maze;

import com.bother.maze.common.AABBoxCollisionVolume;
import com.bother.maze.common.Vector2;

/**
 * Created by Bohdan on 20.03.2015.
 */
public class GameObject extends BaseObject {
    public float width;
    public float height;
    private Vector2 position;
    private Vector2 velocity;
    private AABBoxCollisionVolume collisionVolume;

    public GameObject() {
        position = new Vector2();
        velocity = new Vector2();
        collisionVolume = null;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position.set(position);
    }

    public float getCenterX() {
        return position.x + width / 2f;
    }

    public float getCenterY() {
        return position.y + height / 2f;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity.set(velocity);
    }

    public AABBoxCollisionVolume getCollisionVolume() {
        return collisionVolume;
    }

    public void setCollisionVolume(AABBoxCollisionVolume collisionVolume) {
        this.collisionVolume = collisionVolume;
    }
}
