package com.bother.maze.game;

/**
 * Created by Bohdan on 24.01.2015.
 */
public class GameEnums {

    public enum TileState {
        DARK, LIT, BLOCKER
    }

    public enum TileType {
        FLOOR, WALL, FINISH
    }

    public enum Direction {
        UP(0, -1), DOWN(0, 1),
        LEFT(-1, 0), RIGHT(1, 0),
        UP_LEFT(-1, -1), UP_RIGHT(1, -1),
        DOWN_LEFT(-1, 1), DOWN_RIGHT(1, 1);

        private static final Direction[] cachedValues = values();
        public int x, y;

        Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static Direction[] all() {
            return cachedValues;
        }
    }
}
