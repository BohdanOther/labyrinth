package com.bother.maze.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;

import com.bother.maze.BaseObject;
import com.bother.maze.GameObjectFactory;
import com.bother.maze.LevelManager;
import com.bother.maze.component.Camera;
import com.bother.maze.component.FOVSolver;
import com.bother.maze.component.ShadowCaster;
import com.bother.maze.model.Hunter;
import com.bother.maze.model.Player;

/**
 * High-level setup object for the Labyrinth game engine.
 * This class sets up the core game engine objects and threads.
 * It also passes events to the game thread from the main UI thread.
 */
public class Game {
    // class stuff
    private boolean isRunning;
    private ContextParams params;
    private GameThread gameThread;
    private Thread game;

    private SurfaceView surfaceView;

    private int fps;
    private Paint paint;

    // models
    private Player player;
    private Hunter hunter;

    // components
    private FOVSolver fov;
    private ShadowCaster shadowCaster;

    public Game() {
        isRunning = false;
    }

    /**
     * Creates core game objects.
     * Note that the game does not actually begin running after this function is called
     * (see start() below).
     */
    public void bootstrap(Context context, float scrWidth, float scrHeight) {
        params = new ContextParams();
        params.context = context;
        params.resources = context.getResources();
        params.scrWidth = scrWidth;
        params.scrHeight = scrHeight;
        BaseObject.systemRegistry.contextParams = params;

        // load base systems
        BaseObject.systemRegistry.gameObjectFactory = new GameObjectFactory();
        BaseObject.systemRegistry.inputInterface = new GameInputInterface();
        BaseObject.systemRegistry.camera = new Camera();
        ;

        LevelManager lm = new LevelManager();
        BaseObject.systemRegistry.levelManager = lm;

        // TODO: this is temporary
        //lm.LoadLevelFromFile(params.context, "level_2");
        lm.LoadLevelFromGenerator(100);

        player = (Player) BaseObject.systemRegistry.gameObjectFactory.spawnPlayer(
                lm.getCurrentLevel().getWorld().getStart().x + (GameConstants.TILE_SIZE - GameConstants.PLAYER_SIZE) / 2.0f,
                lm.getCurrentLevel().getWorld().getStart().y + (GameConstants.TILE_SIZE - GameConstants.PLAYER_SIZE) / 2.0f);

        hunter = (Hunter) BaseObject.systemRegistry.gameObjectFactory.spawnHunter(
                lm.getCurrentLevel().getWorld().getStart().x,
                lm.getCurrentLevel().getWorld().getStart().y);

        fov = new FOVSolver(lm.getCurrentLevel().getTiles());
        shadowCaster = new ShadowCaster(lm.getCurrentLevel().getTiles());

        paint = new Paint();
        paint.setColor(Color.RED);

        gameThread = new GameThread(surfaceView, this);
    }

    public void start() {
        if (!isRunning) {
            Log.d("Labyrinth", "Start!");
            game = new Thread(gameThread);
            game.setName("Game");
            game.start();
            isRunning = true;
        } else {
            gameThread.resumeGame();
        }
    }

    public void stop() {
        if (isRunning) {
            Log.d("Labyrinth", "Stop!");
            if (gameThread.isPaused()) {
                gameThread.resumeGame();
            }
            gameThread.stopGame();
            try {
                game.join();
            } catch (InterruptedException e) {
                game.interrupt();
            }
            game = null;
            isRunning = false;
        }
    }

    public void onPause() {
        if (isRunning) {
            gameThread.pauseGame();
        }
    }

    public void onResume() {
        if (isRunning && gameThread.isPaused()) {
            gameThread.resumeGame();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        BaseObject.systemRegistry.inputInterface.setMotionEvent(event);
        return true;
    }

    public void onSurfaceChanged(int width, int height) {
        params.scrWidth = width;
        params.scrHeight = height;
    }

    public void update() {
        BaseObject.systemRegistry.inputInterface.update();

        BaseObject.systemRegistry.levelManager.getCurrentLevel().update();
        player.update();
        //hunter.update();

        BaseObject.systemRegistry.camera.update();

        fov.update();
        //shadowCaster.update(targetObject.centerX(), targetObject.centerY());
    }

    public void render(Canvas canvas) {
        canvas.drawColor(GameConstants.CLEAR_COLOR);

        BaseObject.systemRegistry.levelManager.getCurrentLevel().render(canvas);
        player.render(canvas);
        //shadowCaster.render(canvas, camera);

        BaseObject.systemRegistry.inputInterface.controller.render(canvas);

        displayFps(canvas);
    }

    private void displayFps(Canvas canvas) {
        canvas.drawText(Integer.toString(fps), 20, 20, paint);
    }

    public void setFps(int fps) {
        this.fps = fps;
    }

    public void setSurfaceView(SurfaceView surfaceView) {
        this.surfaceView = surfaceView;
    }

    public void setGameController(final int controllerType) {
        BaseObject.systemRegistry.inputInterface.setController(controllerType);
    }

    public boolean isPaused() {
        return (isRunning && gameThread != null && gameThread.isPaused());
    }
}
