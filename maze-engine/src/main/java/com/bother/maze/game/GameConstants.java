package com.bother.maze.game;

/**
 * Created by Bohdan on 28.01.2015.
 */
public class GameConstants {

    public static final int TILE_SIZE = 64;
    public static final int PLAYER_SIZE = 32;

    public static final int PLAYER_SPEED = 5;
    public static final int TILE_FADE_SPEED = 2;
    public static final int CAMERA_FOLLOW_SPEED = 2;

    public static final int FOV_RADIUS = 10;
    public static final int RENDER_RADIUS = 10;

    /**
     * COLORS
     * 0xAARRGGBB
     */
    public static final int PLAYER_COLOR = 0xFF00A388;
    public static final int WALL_COLOR = 0xFF9b3737;
    public static final int FINISH_COLOR = 0xFF263238;
    public static final int CLEAR_COLOR = 0xFFF4FCFF;
}
