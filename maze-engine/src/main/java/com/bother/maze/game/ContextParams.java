package com.bother.maze.game;

import android.content.Context;
import android.content.res.Resources;

/**
 * Created by Bohdan on 30.01.2015.
 */
public class ContextParams {

    public float scrHeight;
    public float scrWidth;
    public Context context;
    public Resources resources;
}
