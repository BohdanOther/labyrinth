package com.bother.maze.game;

import android.os.SystemClock;
import android.view.MotionEvent;

import com.bother.maze.controller.AbstractGameController;
import com.bother.maze.controller.FingerTraceController;
import com.bother.maze.controller.FixedController;
import com.bother.maze.controller.FloatingController;

/**
 * Filter input layer between game and game view.
 */
public class GameInputInterface {
    public static final int CONTROLLER_TRACE = 0;
    public static final int CONTROLLER_FLOATING = 1;
    public static final int CONTROLLER_FIXED = 2;

    private static final AbstractGameController controllers[] = {
            new FingerTraceController(),
            new FloatingController(),
            new FixedController()
    };

    public AbstractGameController controller = controllers[CONTROLLER_TRACE];

    private MotionEvent motionEvent;

    public GameInputInterface() {
        // to avoid null pointer on first get invoke
        motionEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0);
    }

    public void update() {
        controller.update();
    }

    public void setController(final int type) {
        if (type >= 0 && type <= 2) controller = controllers[type];
    }

    public void setMotionEvent(MotionEvent motionEvent) {
        this.motionEvent = motionEvent;
    }

    public MotionEvent getMotionEvent() {
        return motionEvent;
    }
}
