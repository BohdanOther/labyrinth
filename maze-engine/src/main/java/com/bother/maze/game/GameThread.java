package com.bother.maze.game;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Bohdan on 22.01.2015.
 */
public class GameThread extends Thread {
    private static final String TAG = GameThread.class.getSimpleName();

    // game physics will update this amount per second
    private final int TARGET_UPS = 60;

    // Well, this one is interesting.
    // We could draw as many frames per second as we can.
    // But since our target platform is mobile,
    // fps-lock will save a lot of battery.
    // And 60 FPS is actually more then enough for mobile.
    private final int TARGET_FPS = 60;

    // nanoseconds for each update/draw based on target ups/fps
    final double TIME_BETWEEN_UPDATES = 1000000000 / TARGET_UPS;
    final double TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

    // if we're slow, then we want to skip some draw-frames and update.
    // This sets max amount of updates
    // without rendering the frame.
    final int MAX_RENDER_SKIPS = 5;

    // game engine reference
    private final Game game;

    // view reference for sending stuff back to view
    private SurfaceHolder surfaceHolder;

    private boolean finished;
    private boolean paused;
    private Object pauseLock;

    public GameThread(SurfaceView gameView, final Game game) {
        this.surfaceHolder = gameView.getHolder();
        this.game = game;
        pauseLock = new Object();
        paused = false;
        finished = false;
    }

    @Override
    public void run() {
        Log.d(TAG, "Game loop started");

        long lastUpdateTime = System.nanoTime();
        long lastRenderTime;

        //Simple way of finding FPS.
        long fpsTimer = System.nanoTime();
        int fps = 0;

        Canvas canvas = null;

        finished = false;

        while (!finished) {
            long now = System.nanoTime();
            int updates = 0;

            while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updates < MAX_RENDER_SKIPS) {
                game.update();
                lastUpdateTime += TIME_BETWEEN_UPDATES;
                updates++;
            }

            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder) {
                    //if (canvas != null)
                    game.render(canvas);
                }
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            lastRenderTime = now;

            // count fps
            if (System.nanoTime() - fpsTimer >= 1000000000) {
                game.setFps(fps);
                fps = 0;
                fpsTimer = System.nanoTime();
            } else fps++;

            // fps-lock
            while (now - lastRenderTime < TIME_BETWEEN_RENDERS &&
                    now - lastUpdateTime < TIME_BETWEEN_UPDATES) {
                Thread.yield();

                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                }

                now = System.nanoTime();
            }

            // pause game
            synchronized (pauseLock) {
                // if(paused) - if need to pause other systems
                while (paused) {
                    Log.d("Game Thread", "Pause!");
                    try {
                        pauseLock.wait();
                    } catch (InterruptedException e) {
                        // No big deal if this wait is interrupted.
                    }
                }

            }
        }
    }

    public void stopGame() {
        synchronized (pauseLock) {
            paused = false;
            finished = true;
            pauseLock.notifyAll();
        }
    }

    public void pauseGame() {
        synchronized (pauseLock) {
            paused = true;
        }
    }

    public void resumeGame() {
        synchronized (pauseLock) {
            paused = false;
            pauseLock.notifyAll();
        }
    }

    public boolean isPaused() {
        return paused;
    }
}
