package com.bother.maze.common;

/**
 * Simple axis-aligned bounding box for collision detection.
 * Maybe it's better to create CollisionVolume interface and implement it
 * in order to have different collision volumes like sphere etc
 */
public class AABBoxCollisionVolume {

    private Vector2 topLeft;
    private Vector2 widthHeight;

    public AABBoxCollisionVolume(float posX, float posY, float width, float height) {
        topLeft = new Vector2(posX, posY);
        widthHeight = new Vector2(width, height);
    }

    public boolean isColliding(final AABBoxCollisionVolume collider) {
        return isColliding(this, collider);
    }

    public static boolean isColliding(final AABBoxCollisionVolume first, final AABBoxCollisionVolume second) {
        return first.maxX() > second.minX() &&
                second.maxX() > first.minX() &&
                first.maxY() > second.minY() &&
                second.maxY() > first.minY();
    }

    public static Vector2 accurateDistance(final AABBoxCollisionVolume first, final AABBoxCollisionVolume second) {

        float xDistance = Math.max(
                second.minX() - first.maxX(),
                first.minX() - second.maxX()
        );
        xDistance = xDistance >= 0 ? xDistance : 0;

        float yDistance = Math.max(
                second.minY() - first.maxY(),
                first.minY() - second.maxY()
        );
        yDistance = yDistance >= 0 ? yDistance : 0;

        return new Vector2(xDistance, yDistance);
    }

    public Vector2 accurateDistance(final AABBoxCollisionVolume collider) {
        return accurateDistance(this, collider);
    }

    public void moveTo(Vector2 position) {
        topLeft.set(position);
    }

    public void translate(Vector2 offset) {
        topLeft.add(offset);
    }

    public float minX() {
        return topLeft.x;
    }

    public float minY() {
        return topLeft.y;
    }

    public float maxX() {
        return topLeft.x + widthHeight.x;
    }

    public float maxY() {
        return topLeft.y + widthHeight.y;
    }
}
