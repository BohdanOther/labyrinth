package com.bother.maze.controller;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import com.bother.maze.GameObject;
import com.bother.maze.component.Camera;
import com.bother.maze.game.GameConstants;

/**
 * Our first controller prototype
 * Player will follow your finger
 */
public class FingerTraceController extends AbstractGameController {

    private Paint pointingCircle;

    public FingerTraceController() {
        pointingCircle = new Paint();
        pointingCircle.setFlags(Paint.ANTI_ALIAS_FLAG);
        pointingCircle.setColor(GameConstants.PLAYER_COLOR);
    }

    @Override
    public void update() {
        MotionEvent event = systemRegistry.inputInterface.getMotionEvent();
        GameObject target = systemRegistry.camera.getTargetObject();
        center.set(target.getCenterX(), target.getCenterY());

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                final float dx = event.getX(0) - center.x + systemRegistry.camera.position.x;
                final float dy = event.getY(0) - center.y + systemRegistry.camera.position.y;
                delta.set(dx, dy);

                if (delta.length2() > 900) {
                    delta.normalize();
                } else {
                    delta.zero();
                }
                break;

            case MotionEvent.ACTION_UP:
                delta.zero();
                break;
        }
    }

    @Override
    public void render(Canvas canvas) {
        Camera camera = systemRegistry.camera;
        float renderX = center.x + delta.x * GameConstants.PLAYER_SIZE * 2 - camera.position.x;
        float renderY = center.y + delta.y * GameConstants.PLAYER_SIZE * 2 - camera.position.y;
        canvas.drawCircle(renderX, renderY, 4, pointingCircle);
    }
}
