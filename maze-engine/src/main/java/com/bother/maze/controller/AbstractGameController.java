package com.bother.maze.controller;

import android.graphics.Canvas;

import com.bother.maze.BaseObject;
import com.bother.maze.common.Vector2;

/**
 * Abstract game controller.
 */
public abstract class AbstractGameController extends BaseObject {
    // center of controller
    protected Vector2 center;
    // normalized delta offset from center
    protected Vector2 delta;

    public AbstractGameController() {
        center = new Vector2();
        delta = new Vector2();
    }

    public void render(Canvas canvas) {
        // Base class renders nothing
    }

    public Vector2 getVelocity() {
        return delta;
    }
}
