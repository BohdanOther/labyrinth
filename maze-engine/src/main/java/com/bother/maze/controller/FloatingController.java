package com.bother.maze.controller;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

/**
 * Controller will appear in place that is being touched
 * and is there until touch released.
 * Comfortable for huge tablet screens.
 */
public class FloatingController extends AbstractGameController {

    private boolean touched;

    private float radius = 100;
    private float deadRadius = 20;
    private float visibleRadius;
    private float fadeSpeed = 5f;

    private Paint innerCircle;
    private Paint outerCircle;

    public FloatingController() {
        innerCircle = new Paint();
        innerCircle.setFlags(Paint.ANTI_ALIAS_FLAG);
        innerCircle.setColor(0xFFa0d5ff);

        outerCircle = new Paint();
        outerCircle.setFlags(Paint.ANTI_ALIAS_FLAG);
        outerCircle.setARGB(20, 0, 0, 0);
    }

    @Override
    public void update() {
        MotionEvent event = systemRegistry.inputInterface.getMotionEvent();

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                center.set(event.getX(0), event.getY(0));
                touched = true;
                break;

            case MotionEvent.ACTION_MOVE:
                final float dx = event.getX(0) - center.x;
                final float dy = event.getY(0) - center.y;
                delta.set(dx, dy);

                if (delta.length2() < deadRadius * deadRadius) {
                    delta.zero();
                } else if (delta.length2() < radius * radius) {
                    delta.divide(radius);
                } else {
                    delta.normalize();
                }
                break;

            case MotionEvent.ACTION_UP:
                touched = false;
                delta.zero();
                break;
        }

        if (touched && visibleRadius < radius) {
            visibleRadius += fadeSpeed;
        } else if (!touched && visibleRadius > 0) {
            visibleRadius -= fadeSpeed;
        }
    }

    @Override
    public void render(Canvas canvas) {
        if (visibleRadius > 0) {
            canvas.drawCircle(center.x, center.y, visibleRadius, outerCircle);
            canvas.drawCircle(center.x + delta.x * radius + 2, center.y + delta.y * radius + 1, 21, outerCircle);
            canvas.drawCircle(center.x + delta.x * radius, center.y + delta.y * radius, 20, innerCircle);
        }
    }
}
