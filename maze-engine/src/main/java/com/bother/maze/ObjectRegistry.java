package com.bother.maze;

import com.bother.maze.component.Camera;
import com.bother.maze.game.ContextParams;
import com.bother.maze.game.GameInputInterface;

/**
 * ObjectRegistry holds collection of game-wide singleton objects.
 * Later here will be
 * - sound system manager
 * + level manager
 * - notification manager
 * - hud system
 * + input system
 * - texture library
 * - render system
 * - any kind of pool(if there will be)
 * + context parameters
 */
public class ObjectRegistry {
    public Camera camera;
    public ContextParams contextParams;
    public GameObjectFactory gameObjectFactory;
    public GameInputInterface inputInterface;
    public LevelManager levelManager;
}
