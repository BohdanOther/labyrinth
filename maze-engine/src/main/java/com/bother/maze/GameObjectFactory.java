package com.bother.maze;

import com.bother.maze.common.AABBoxCollisionVolume;
import com.bother.maze.common.Vector2;
import com.bother.maze.game.GameConstants;
import com.bother.maze.game.GameEnums;
import com.bother.maze.model.Hunter;
import com.bother.maze.model.Player;
import com.bother.maze.model.Tile;

/**
 * Created by Bohdan on 19.03.2015.
 */
public class GameObjectFactory {

    public enum GameObjectType {
        INVALID(-1),

        TILE_FLOOR(0),
        TILE_WALL(1),
        TILE_EXIT(2),

        PLAYER(3),
        HUNTER(4);
        private final static GameObjectType[] cachedValues = values();
        private final int id;

        GameObjectType(int id) {
            this.id = id;
        }

        public static GameObjectType idToType(int id) {
            GameObjectType foundType = INVALID;
            for (int x = 0; x < cachedValues.length; x++) {
                GameObjectType type = cachedValues[x];
                if (type.id == id) {
                    foundType = type;
                    break;
                }
            }
            if (id == 3) foundType = TILE_FLOOR;
            return foundType;
        }

        public int id() {
            return id;
        }
    }

    public GameObject spawn(GameObjectType type, float x, float y) {
        GameObject newObject = null;
        switch (type) {
            case PLAYER:
                newObject = spawnPlayer(x, y);
                break;
            case HUNTER:
                newObject = spawnHunter(x, y);
                break;
            case TILE_WALL:
                newObject = spawnTileWall(x, y);
                break;
            case TILE_FLOOR:
                newObject = spawnTileFloor(x, y);
                break;
            case TILE_EXIT:
                newObject = spawnTileExit(x, y);
                break;
        }

        return newObject;
    }

    public void spawnFromWorld(int[][] world) {
        //GameObjectManager manager = sSystemRegistry.gameObjectManager;
//        for (int y = 0; y < world.getHeight(); y++) {
//            for (int x = 0; x < world.getWidth(); x++) {
//                int index = world[x][y];
//                if (index != -1) {
//                    GameObjectType type = GameObjectType.idToType(index);
//                    if (type != GameObjectType.INVALID) {
//                        final float worldX = x * GameConstants.TILE_SIZE;
//                        final float worldY = worldHeight - ((y + 1) * GameConstants.TILE_SIZE);
//                        GameObject object = spawn(type, worldX, worldY);
//                        if (object != null) {
////                            if (object.height < tileHeight) {
////                                // make sure small objects are vertically centered in their
////                                // tile.
////                                object.getPosition().y += (tileHeight - object.height) / 2.0f;
////                            }
////                            if (object.width < tileWidth) {
////                                object.getPosition().x += (tileWidth - object.width) / 2.0f;
////                            } else if (object.width > tileWidth) {
////                                object.getPosition().x -= (object.width - tileWidth) / 2.0f;
////                            }
//                            //manager.add(object);
//                            if (type == GameObjectType.PLAYER) {
//                                //manager.setPlayer(object);
//                            }
//                        }
//                    }
//                }
//            }
//        }

    }

    public GameObject spawnPlayer(float x, float y) {
        GameObject object = new Player();

        object.getPosition().set(x, y);
        object.width = GameConstants.PLAYER_SIZE;
        object.height = GameConstants.PLAYER_SIZE;

        AABBoxCollisionVolume collisionVolume =
                new AABBoxCollisionVolume(x, y, GameConstants.PLAYER_SIZE, GameConstants.PLAYER_SIZE);

        object.setCollisionVolume(collisionVolume);

        BaseObject.systemRegistry.camera.setTargetObject(object);

        return object;
    }

    public GameObject spawnTileWall(float x, float y) {
        GameObject object = new Tile(GameEnums.TileType.WALL);

        object.getPosition().set(x, y);
        object.width = GameConstants.TILE_SIZE;
        object.height = GameConstants.TILE_SIZE;

        AABBoxCollisionVolume collisionVolume =
                new AABBoxCollisionVolume(x, y, GameConstants.TILE_SIZE, GameConstants.TILE_SIZE);
        object.setCollisionVolume(collisionVolume);

        return object;
    }

    public GameObject spawnTileExit(float x, float y) {
        GameObject object = new Tile(GameEnums.TileType.FINISH);

        object.getPosition().set(x, y);
        object.width = GameConstants.TILE_SIZE;
        object.height = GameConstants.TILE_SIZE;

        AABBoxCollisionVolume collisionVolume = new AABBoxCollisionVolume(
                x + GameConstants.TILE_SIZE / 4,
                y + GameConstants.TILE_SIZE / 4,
                GameConstants.TILE_SIZE / 2,
                GameConstants.TILE_SIZE / 2);

        //object.setCollisionVolume(collisionVolume);

        return object;
    }

    public GameObject spawnTileFloor(float x, float y) {
        GameObject object = new Tile(GameEnums.TileType.FLOOR);

        object.getPosition().set(x, y);
        object.width = GameConstants.TILE_SIZE;
        object.height = GameConstants.TILE_SIZE;

        return object;
    }

    public GameObject spawnHunter(float x, float y) {
        GameObject object = new Hunter();
        object.setPosition(new Vector2(x, y));

        return object;
    }
}
