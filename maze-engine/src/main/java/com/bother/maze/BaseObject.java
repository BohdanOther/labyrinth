package com.bother.maze;

/**
 * Core object from which most objects are derived.
 * Anything that requires an update per second should be
 * derived from BaseObject.
 * Also BaseObject hold object-wide system registry.
 */
public class BaseObject {
    public static ObjectRegistry systemRegistry = new ObjectRegistry();

    public void update() {
        // base object does nothing
    }
}
