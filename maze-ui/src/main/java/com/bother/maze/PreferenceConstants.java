package com.bother.maze;

/**
 * Class to store all preference names in one place
 * for accessing shared preferences.
 */
public class PreferenceConstants {
    public static final String PREFERENCE_CONTROLLER = "controller";


    public static final String PREFERENCE_NAME = "bother.labyrinth.preferences";
}
