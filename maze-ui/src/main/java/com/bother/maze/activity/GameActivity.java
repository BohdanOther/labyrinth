package com.bother.maze.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.bother.maze.PreferenceConstants;
import com.bother.maze.R;
import com.bother.maze.game.Game;


/**
 * Core activity for the game.  Sets up a surface view, bootstraps
 * the game engine, and manages UI events.
 * Also will manage game progression,
 * transitioning to other activities, save game and other.
 */
public class GameActivity extends Activity implements SurfaceHolder.Callback {

    private Game game;
    private SurfaceView surfaceView;

    private long mLastTouchTime = 0L;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.game_surface_view);
        surfaceView.setFocusable(true);
        surfaceView.getHolder().addCallback(this);
        this.surfaceView = surfaceView;

        // use this later to get wide-game info
        // like targetObject statistics and etc.
        // don't resolve game settings here, do it in onResume instead.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        game = new Game();
        game.setSurfaceView(surfaceView);
        game.bootstrap(this, dm.widthPixels, dm.heightPixels);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Labyrinth", "onResume");

        // game preference may change during game
        // check here for sound, controller and other in-game settings
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        int controllerType = Integer.valueOf(sharedPref.getString(PreferenceConstants.PREFERENCE_CONTROLLER, "0"));

        game.setGameController(controllerType);

        game.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Labyrinth", "onPause");
        game.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d("Labyrinth", "onDestroy");
        game.stop();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        game.stop();
        super.onBackPressed();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("Game surface", "surfaceCreated");
        game.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d("Game surface", "surfaceChanged");
        // TODO: is this good ? or block orientation in game activity
        onPause();
        game.onSurfaceChanged(width, height);
        onResume();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("Game surface", "surfaceDestroyed");
        game.stop();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!game.isPaused()) {
            game.onTouchEvent(event);

            final long time = System.currentTimeMillis();
            if (event.getAction() == MotionEvent.ACTION_MOVE && time - mLastTouchTime < 32) {
                // Sleep so that the main thread doesn't get flooded with UI events.
                try {
                    Thread.sleep(32);
                } catch (InterruptedException e) {
                    // No big deal if this sleep is interrupted.
                }
            }
            mLastTouchTime = time;
        }
        return true;
    }
}
