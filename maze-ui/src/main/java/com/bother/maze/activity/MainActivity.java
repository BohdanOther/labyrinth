package com.bother.maze.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bother.maze.R;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void openGameViewButtonClick(View view) {
        Intent openGameActivityIntent = new Intent(this, GameActivity.class);

        startActivity(openGameActivityIntent);
    }

    public void openSettingsActivityButtonClick(View view) {
        Intent openSettingsIntent = new Intent(this, SettingsActivity.class);

        startActivity(openSettingsIntent);
    }

    public void exitGameButtonClick(View view) {
        finish();
    }
}
