package com.bother.maze.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.View;
import android.widget.RadioGroup;

import com.bother.maze.PreferenceConstants;
import com.bother.maze.R;
import com.bother.maze.game.GameInputInterface;

/**
 * Created by urion on 05.02.15.
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
