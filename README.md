### Android Studio usefull hotkeys ###
* double tap "Shift" // global search
* Ctrl + Tab // switch classes
* Ctrl+Alt+L // auto format code
* Ctrl+Alt+O // delete unused imports
* Ctrl+/   // comment/uncomment selected lines


### Debug, deploy and run over Wi-fi ###

    * make sure debugging is working "adb devices" . in cmd type:
*     adb tcpip 5555
*     adb connect <DEVICE_IP_ADDRESS>:5555
*     Disconnect USB and proceed with wireless debugging.
*     adb -s <DEVICE_IP_ADDRESS>:5555 usb to switch back when done.

* DEVICE_IP_ADDRESS - go to device's Settings -> About tablet(telephone) -> Status

* to make "adb" work in cmd you should be in adb folder "C:\Users\Bohdan\AppData\Local\Android\sdk\platform-tools" (windows) or have that path added to Path  in environmental variables;




### GIT ###

## To commit and push your changes to server ##

* git status // for see current status 
* git add --all // to apply all changes 
* git status // if you want - you should see all green messages 
* git commit -m "your good and reasonable message here" 
* git push 


## **To update your local repository (this will erase all uncommited progress!)** : ##
* git fetch 
* git pull 

## **To update, but save you progress** ##
* git add --all
* git commit -m "My message"
* git fetch
* git pull

## To erase changes and back up to last server commit ##
* git checkout // erase all changes in project
* git checkout c:/users/USER/...../ayp-port/.../yourclass.java // restore only one file